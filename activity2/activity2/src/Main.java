import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {

        //ARRAY
        int[] primeNumbers = new int[5];
        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        //System.out.println(Arrays.toString(primeNumbers));
        System.out.println("The first prime number is: "+ primeNumbers[0]);
        System.out.println("The second prime number is: "+ primeNumbers[1]);
        System.out.println("The third prime number is: "+ primeNumbers[2]);
        System.out.println("The fourth prime number is: "+ primeNumbers[3]);
        System.out.println("The fifth prime number is: "+ primeNumbers[4]);

        //ARRAYLIST
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: "+ friends);

        //HASHMAP
        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: "+ inventory);

    }
}